#!/usr/bin/env node
const fs = require('fs');
fs.readFile('script.js', 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	let script = data;
	let index = 1;

	script = script.replace(/aloes\([0-9]+\)/g, 'aloes()');

	while (script.includes('aloes()')) {
		script = script.replace('aloes()', 'aloes(' + index + ')');
		index++;
	}

	fs.writeFile('_script.js', script, 'utf8', function (err) {
		if (err) return console.log(err);
	});
});
