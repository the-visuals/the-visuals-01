/* global monogatari */

// Define the messages used in the game.
monogatari.action ('message').messages ({
	'Help': {
		title: 'Help',
		subtitle: 'Some useful Links',
		body: `
			<p><a href='https://developers.monogatari.io/documentation/'>Documentation</a> - Everything you need to know.</p>
			<p><a href='https://monogatari.io/demo/'>Demo</a> - A simple Demo.</p>
		`
	}
});

// Define the notifications used in the game
monogatari.action ('notification').notifications ({
	'Welcome': {
		title: 'Welcome',
		body: 'This is the Monogatari VN Engine',
		icon: ''
	}
});

// Credits of the people involved in the creation of this awesome game
monogatari.configuration ('credits', {

});

// Define the Particles JS Configurations used in the game
monogatari.action ('particles').particles ({

});

// Define the images that will be available on your game's image gallery
monogatari.assets ('gallery', {

});

// Define the music used in the game.
monogatari.assets ('music', {
	deepBlue: 'bensound-deepblue.mp3',
	acousticBreeze: 'bensound-acousticbreeze.mp3',
	scifi: 'bensound-scifi.mp3',
	memories: 'bensound-memories.mp3',
	sadDay: 'bensound-sadday.mp3'
});

// Define the voice files used in the game.
monogatari.assets ('voices', {});

// Define the sounds used in the game.
monogatari.assets ('sounds', {
	loudKnock: 'loudKnock.mp3',
	louderKnock: 'louderKnock.mp3',
	softKnock: 'softKnock.mp3',
	cameraShutter: 'cameraShutter.mp3',
	notification: 'notification.mp3',
	keyboardLogin: 'keyboardLogin.mp3',
	slammingTheDoor: 'slammingTheDoor.mp3',
	openingTheDoor: 'openingTheDoor.mp3',
	slowDoorOpening: 'slowDoorOpen.mp3',
});

// Define the videos used in the game.
monogatari.assets ('videos', {

});

// Define the images used in the game.
monogatari.assets ('images', {

});

function canUseWebP () {
	var elem = document.createElement('canvas');
	if (elem.getContext && elem.getContext('2d')) {
		return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
	}
	return false;
}
var webPSupported = canUseWebP();

// Define the backgrounds for each scene.
monogatari.assets ('scenes', {
	// bathroom: webPSupported ? 'bathroom.webp' : 'bathroom.png',
	// beachDaySimple: webPSupported ? 'beach_day_simple.webp' : 'beach_day_simple.png',
	// beachDayUmbrella: webPSupported ? 'beach_day_umbrella.webp' : 'beach_day_umbrella.png',
	// beachSunsetSimple: webPSupported ? 'beach_sunset_simple.webp' : 'beach_sunset_simple.png',
	bedroomNight: webPSupported ? 'bedroom_night.webp' : 'bedroom_night.png',
	bedroom: webPSupported ? 'bedroom.webp' : 'bedroom.png',
	busStop: webPSupported ? 'bus_stop.webp' : 'bus_stop.png',
	// cafe: webPSupported ? 'cafe.webp' : 'cafe.png',
	// classroomEvening: webPSupported ? 'classroom_evening.webp' : 'classroom_evening.png',
	// classroom: webPSupported ? 'classroom.webp' : 'classroom.png',
	hallwayHouse: webPSupported ? 'hallway_house.webp' : 'hallway_house.jpg',
	hallwayHouseNight: webPSupported ? 'hallway_house_night.webp' : 'hallway_house_night.png',
	hallwaySchool: webPSupported ? 'hallway_school.webp' : 'hallway_school.png',
	hallway: webPSupported ? 'hallway.webp' : 'hallway.jpg',
	// hallway2: webPSupported ? 'hallway2.webp' : 'hallway2.jpg',
	// hallwayNight: webPSupported ? 'hallway_night.webp' : 'hallway_night.jpg',
	// housesEvening: webPSupported ? 'houses_evening.webp' : 'houses_evening.png',
	// housesNight: webPSupported ? 'houses_night.webp' : 'houses_night.png',
	houses: webPSupported ? 'houses.webp' : 'houses.png',
	// japaneseSetting: webPSupported ? 'japanese_setting.webp' : 'japanese_setting.png',
	kitchen: webPSupported ? 'kitchen.webp' : 'kitchen.png',
	// livingRoom: webPSupported ? 'living_room.webp' : 'living_room.png',
	livingRoomNight: webPSupported ? 'living_room_night.webp' : 'living_room_night.png',
	// moonLinkFence: webPSupported ? 'moon_link_fence.webp' : 'moon_link_fence.png',
	// moonOverField: webPSupported ? 'moon_over_field.webp' : 'moon_over_field.png',
	// moonPark: webPSupported ? 'moon_park.webp' : 'moon_park.png',
	// moon: webPSupported ? 'moon.webp' : 'moon.png',
	park: webPSupported ? 'park.webp' : 'park.png',
	// schoolCafeteria: webPSupported ? 'school_cafeteria.webp' : 'school_cafeteria.png',
	// schoolEntranceEvening: webPSupported ? 'school_entrance_evening.webp' : 'school_entrance_evening.png',
	schoolEntrance: webPSupported ? 'school_entrance.webp' : 'school_entrance.png',
	// schoolRooftop: webPSupported ? 'school_rooftop.webp' : 'school_rooftop.png',
	// stairs: webPSupported ? 'stairs.webp' : 'stairs.png',
	kitchenNight: webPSupported ? 'kitchen_night.webp' : 'kitchen_night.png',
	end: webPSupported ? 'end.webp' : 'end.png',
});


// Define the Characters
monogatari.characters ({
	c: {
		name: 'Charlie',
		color: '#f94e5c',
		directory: 'charlie',
		sprites :{
			stressedSchoolUniform: webPSupported ? 'stressedSchoolUniform.webp' : 'stressedSchoolUniform.png',
			shockedSchoolUniform: webPSupported ? 'shockedSchoolUniform.webp' : 'shockedSchoolUniform.png',
			fullSmileSchoolUniform: webPSupported ? 'fullSmileSchoolUniform.webp' : 'fullSmileSchoolUniform.png',
			blushSchoolUniform: webPSupported ? 'blushSchoolUniform.webp' : 'blushSchoolUniform.png',
			irritatedSchoolUniform: webPSupported ? 'irritatedSchoolUniform.webp' : 'irritatedSchoolUniform.png',
			angrySchoolUniform: webPSupported ? 'angrySchoolUniform.webp' : 'angrySchoolUniform.png',
			sadSchoolUniform: webPSupported ? 'sadSchoolUniform.webp' : 'sadSchoolUniform.png',
			sadWithTearsSchoolUniform: webPSupported ? 'sadWithTearsSchoolUniform.webp' : 'sadWithTearsSchoolUniform.png',
			excitedPJ: webPSupported ? 'excitedPJ.webp' : 'excitedPJ.png',
			bububuPJ: webPSupported ? 'bububuPJ.webp' : 'bububuPJ.png',
			blushPJ: webPSupported ? 'blushPJ.webp' : 'blushPJ.png',
			shockedPJ: webPSupported ? 'shockedPJ.webp' : 'shockedPJ.png',
			smilePJ: webPSupported ? 'smilePJ.webp' : 'smilePJ.png',
			sadPJ: webPSupported ? 'sadPJ.webp' : 'sadPJ.png',
			sadWithTearsPJ: webPSupported ? 'sadWithTearsPJ.webp' : 'sadWithTearsPJ.png',
			gloomyPJ: webPSupported ? 'gloomyPJ.webp' : 'gloomyPJ.png',
			sadEyesDownNormalClothes: webPSupported ? 'sadEyesDownNormalClothes.webp' : 'sadEyesDownNormalClothes.png',
			sadderEyesDownNormalClothes: webPSupported ? 'sadderEyesDownNormalClothes.webp' : 'sadderEyesDownNormalClothes.png',
			cryingNormalClothes: webPSupported ? 'cryingNormalClothes.webp' : 'cryingNormalClothes.png',
			afraidNormalClothes: webPSupported ? 'afraidNormalClothes.webp' : 'afraidNormalClothes.png',
			livelessNormalClothes: webPSupported ? 'livelessNormalClothes.webp' : 'livelessNormalClothes.png',
		}
	},
	j: {
		name: 'Jo',
		color: '#00bfff',
		directory: 'jo',
		sprites :{
			angrySchoolUniform: webPSupported ? 'angrySchoolUniform.webp' : 'angrySchoolUniform.png',
			sadSchoolUniform: webPSupported ? 'sadSchoolUniform.webp' : 'sadSchoolUniform.png',
			fullSmileSchoolUniform: webPSupported ? 'fullSmileSchoolUniform.webp' : 'fullSmileSchoolUniform.png',
			smirkPJ: webPSupported ? 'smirkPJ.webp' : 'smirkPJ.png',
			fullSmilePJ: webPSupported ? 'fullSmilePJ.webp' : 'fullSmilePJ.png',
			shockedPJ: webPSupported ? 'shockedPJ.webp' : 'shockedPJ.png',
			sadPJ: webPSupported ? 'sadPJ.webp' : 'sadPJ.png',
			worriedNormalClothes: webPSupported ? 'worriedNormalClothes.webp' : 'worriedNormalClothes.png',
			sadNormalClothes: webPSupported ? 'sadNormalClothes.webp' : 'sadNormalClothes.png',
			terrifiedNormalClothes: webPSupported ? 'terrifiedNormalClothes.webp' : 'terrifiedNormalClothes.png',
			smileNormalClothes: webPSupported ? 'smileNormalClothes.webp' : 'smileNormalClothes.png',
			shockedNormalClothes: webPSupported ? 'shockedNormalClothes.webp' : 'shockedNormalClothes.png',
			neutralNormalClothes: webPSupported ? 'neutralNormalClothes.webp' : 'neutralNormalClothes.png',
			smirkNormalClothes: webPSupported ? 'smirkNormalClothes.webp' : 'smirkNormalClothes.png',
		}
	},
	u1: {
		name: 'Uczennica 1',
		color: '#d3ce1a',
		directory: 'npc',
		sprites :{
			maliciousSchoolUniform: webPSupported ? 'npc_1_maliciousSchoolUniform.webp' : 'npc_1_maliciousSchoolUniform.png',
			upsetSchoolUniform: webPSupported ? 'npc_1_upsetSchoolUniform.webp' : 'npc_1_upsetSchoolUniform.png',
		}
	},
	u2: {
		name: 'Uczennica 2',
		color: '#e27a4f',
		directory: 'npc',
		sprites :{
			maliciousSchoolUniform: webPSupported ? 'npc_2_maliciousSchoolUniform.webp' : 'npc_2_maliciousSchoolUniform.png',
			sadSchoolUniform: webPSupported ? 'npc_2_sadSchoolUniform.webp' : 'npc_2_sadSchoolUniform.png',
		}
	},
	m: {
		name: 'Mama',
		color: '#c4c4c4',
		directory: 'npc',
		sprites :{
		}
	},
	o: {
		name: 'Ojciec',
		color: '#c4c4c4',
		directory: 'npc',
		sprites :{
		}
	},
});

function playVoice (actor, index) {
	return `play voice ${actor}_${String(index).padStart(3, '0')}.mp3`;
	// return function () {return true;};
}

function stopVoice () {
	return 'stop voice';
}

function ruriri (index) {
	return playVoice('ruriri', index);
}

function aloes (index) {
	return playVoice('aloes', index);
}

function yudeko (index) {
	return playVoice('yudeko', index);
}

function hikari (index) {
	return playVoice('hikari', index);
}

var characters = {
	j: 'aloes'
};
function reversibleTwoWay (func) {
	return {'Function':{
		'Apply': function () {
			return func();
		},
		'Reverse': function () {
			return func();
		}
	}};
}

function say (character, index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			return monogatari.run(playVoice(characters[character] ? characters[character] : character, index), false);
		});
		monogatari.run(`${character} ${text}`, false);
		return false;
	});
}

monogatari.script ({
	// The game starts here.
	'Start': [
		'play music deepBlue with loop fade 3',
		'show scene hallwayHouseNight with fadeIn',
		'show character j worriedNormalClothes at center',
		'play sound softKnock',
		'wait 500',
		say('j', 1, 'Nie, to na pewno jakaś pomyłka.'),
		say('j', 2, 'Naprawdę, można by pomyśleć, że popadam w obłęd.'),
		say('j', 3, 'Tak to się zaczyna, od cichych odgłosów, do coraz głośniejszych, które potem niepostrzeżenie przechodzą w ludzką mowę i ani się człowiek obejrzy, je śniadanie razem z nieżyjącą już od 20 lat prababką.'),
		say('j', 4, 'Zdecydowanie aż tak źle ze mną nie jest.'),
		say('j', 5, 'Zachowuję jedynie... podstawowe środki ostrożności.'),
		say('j', 6, 'Charlie na pewno by to pochwaliła.'),
		say('j', 7, 'Grunt to bezpieczeństwo.'),
		say('j', 8, 'Prawda, Charlie?'),
		'play sound louderKnock',
		'show character j terrifiedNormalClothes at center',
		'...',
		'...',
		say('j', 9, 'Nie, nie, nie. To na pewno pomyłka.'),
		'play sound loudKnock',
		'...',
		'...',
		'...',
		say('j', 10, 'Nie, to...'),
		say('j', 11, 'Przecież...'),
		say('j', 12, 'To, nie...'),
		say('j', 13, 'Spokojnie, Jo, spokojnie.'),
		say('j', 14, 'Oddychaj.'),
		say('j', 15, 'Nie myśl o tym.'),
		say('j', 16, 'Motylki.'),
		say('j', 17, 'Kwiatki.'),
		say('j', 18, 'Kwiatki...'),
		'hide character j terrifiedNormalClothes',
		'show scene #000 with fadeIn',
		'stop music deepBlue with fade 1',
		'wait 1000',
		'jump Chapter1',
	],

	Chapter1: [
		'<span class="middle"><em>Trzy miesiące wcześniej.</em></span>',
		'show scene hallway with fadeIn',
		'play music acousticBreeze with loop fade 10',
		'show character j neutralNormalClothes at center',
		say('j', 19, '<em>Po sesji dostałam bukiet lilii.</em>'),
		say('j', 20, '<em>Pamiętam, że coś mnie wtedy tknęło.</em>'),
		say('j', 21, '<em>Lilie, kwiaty, których rzadko używa się w ramach podziękowań czy toastów.</em>'),
		say('j', 22, '<em>Przywodzą na myśl... zmarłych.</em>'),
		say('j', 23, '<em>Ustawiłam kwiaty pod odpowiednim kątem, aparat cicho kliknął.</em>'),
		'play sound cameraShutter',
		'show character j smileNormalClothes at center',
		say('j', 24, 'Ciekawe, czy Charlie znowu będzie się śmiać.'),
		say('j', 25, 'Stwierdzi, że przynajmniej lepiej kwiaty niż czekoladki, po których przez tydzień musi oglądać, jak siłą woli wytężoną do maksimum staram się ograniczać do jednej dziennie.'),
		say('j', 26, 'Po to tylko, by trzeciego dnia zjeść całe pudełko, a następnie przez kolejne kilka godzin zadręczać się internetowym kardio z powodu wyrzutów sumienia.'),
		say('j', 27, 'Ach, Charlie.'),
		'show character j sadNormalClothes at center',
		say('j', 28, 'Czasami jej zazdroszczę.'),
		say('j', 29, 'Kalorie, brzuszki, herbata poprawiająca metabolizm – to dla niej całkowita abstrakcja, zupełnie inna rzeczywistość.'),
		say('j', 30, 'Ciekawe, jak to jest...'),
		say('j', 31, 'Od tak dawna żyję w tym świecie ciągłych sesji, wyjazdów, walk o kolejną okładkę, że chyba nie jestem już w stanie wyobrazić sobie innej siebie.'),
		'show character j neutralNormalClothes at center',
		say('j', 32, 'I... nie chcę.'),
		say('j', 33, 'Przecież każda sekunda motywacji jest na wagę złota.'),
		say('j', 34, 'Trzeba zawsze iść do przodu, po zrealizowanych celach przyjdą kolejne, większe, nie ma czasu na odpoczynek.'),
		'show character j worriedNormalClothes at center',
		say('c', 31, '"Cześć, tu Charlie. Jeśli to słyszysz, to znaczy, że chwilowo nie mogę rozmawiać. Zostaw wiadomość, przesłucham ją, kiedy tylko będę mogła. Dziękuję!"'),
		say('j', 35, 'Dziwne.'),
		say('j', 36, 'Charlie o tej porze nie powinna spać, jestem przecież jedyną osobą, u której włączyła powiadomienia przychodzących wiadomości.'),
		say('j', 37, 'Zawsze odpisuje najszybciej, jak tylko jest w stanie...'),
		say('j', 38, 'Nie, dość już tych obsesyjnych myśli.'),
		'show character j smirkNormalClothes at center',
		say('j', 39, 'Nie mogę tak łatwo dać się w to wpędzić.'),
		say('j', 40, 'Zwłaszcza teraz, kiedy już tak dobrze mi idzie.'),
		say('j', 41, 'Równo 30 ostatnich nocy...'),
		say('j', 42, 'Dziś zamierzam świętować miesiąc. Nic wyjątkowego, ale specjalnie na tę okazję zostawiłam sobie paczkę ciastek z zeszłego miesiąca...'),
		say('j', 43, '...udało mi się zasnąć bez budzenia się w środku nocy.'),
		say('j', 44, 'Jakoś tak... w nocy czuję się nieswojo.'),
		say('j', 45, 'Zawsze upewniam się, że idę spać, kiedy sąsiedzi z góry jeszcze hałasują.'),
		say('j', 46, 'No w każdym razie – te czasy odeszły w niepamięć.'),
		'show character j smileNormalClothes at center',
		say('j', 47, 'Ja, Jo Stradford, nie mogę sobie pozwolić na żadne chwile słabości!'),
		'...',
		say('j', 48, 'Hm. Wciąż brak odpowiedzi.'),
		'show character j sadNormalClothes at center',
		say('j', 49, 'To się wcześniej nie zdarzało.'),
		say('j', 50, 'Chociaż rzeczywiście, nie pożegnałyśmy się w zbyt dobrej atmosferze.'),
		say('j', 51, 'O wyjeździe dowiedziałam się w ostatniej chwili, takie są kaprysy francuskich fotografów. Boże, strzeż mnie od nich, nigdy więcej.'),
		'show character j worriedNormalClothes at center',
		say('j', 52, 'Charlie dosyć kiepsko to przyjęła.'),
		say('j', 53, 'Nie lubi, gdy wyjeżdżam.'),
		say('j', 54, 'Zwłaszcza na dłużej.'),
		say('j', 55, 'Powiedziałam jej o tym w tym małym parku, który ostatnio odkryłam.'),
		say('j', 56, 'Pomyślałam sobie wtedy, że to świetne miejsce na spontaniczną randkę – choć może trochę mniej na spontaniczne wiadomości o trzytygodniowym wyjeździe na Lazurowe Wybrzeże, podczas gdy z Charlie...'),
		say('j', 57, 'No, powiedzmy, że nie dopisywał jej ostatnio szczególnie dobry nastrój.'),
		say('j', 58, 'Znając jej zmienne humory, pewnie obraziła się na mnie i czeka na wieczorną serenadę pełną skruchy.'),
		say('j', 59, 'Chociaż z nas dwóch to ona wykazuje zdecydowanie więcej talentu muzycznego.'),
		say('j', 60, 'Ja, jak by delikatnie to powiedzieć...'),
		'play sound notification',
		'show character j shockedNormalClothes at center',
		'...',
		say('j', 61, ' <em>Pamiętam, jak wtedy machinalnie sięgnęłam po telefon, przesunęłam palcami po gładkim ekranie, zdejmując blokadę.</em>'),
		'show character j neutralNormalClothes at center',
		say('j', 62, ' Ach, to tylko generyczne powiadomienie z wiadomoś...'),
		'hide character j neutralNormalClothes at center',
		'show scene #000 with fadeIn',
		'stop music acousticBreeze with fade 1',
		say('j', 63, ' <em>Na chwilę wszystko zrobiło się czarne.</em>'),
		say('j', 64, ' <em>Pamiętam, że nagle zdałam sobie sprawę z krwi, która głośno szumiała mi w uszach.</em>'),
		'<em>"...17-letnia uczennica liceum Maple Park znaleziona nad ranem w łazience..."</em>',
		'show scene hallway',
		'show character j terrifiedNormalClothes at center',
		say('j', 65, ' Zaraz... nie, przecież... no właśnie.'),
		say('j', 66, ' Przecież Charlie skończyła w tym roku 18 lat. Nie 17.'),
		say('j', 67, ' To pomyłka, absurdalny zbieg okoliczności, przyprawiający o dreszcze.'),
		say('j', 68, ' Przecież Charlie nie podcięłaby sobie żył.'),
		'show scene #000 with fadeIn',
		'jump Chapter2',
	],

	Chapter2: [
		'show scene #000 with fadeIn',
		'wait 1500',
		'show scene houses',
		'play music acousticBreeze with loop fade 10',
		'show character j neutralNormalClothes at center',
		say('j', 69, ' Charlie nadal jest obrażona.'),
		say('j', 70, ' Zapamiętała najwyraźniej, kiedy wracam z Francji do domu i od tego czasu przestała pojawiać się w szkole.'),
		say('j', 71, ' Naprawdę, to posunęło się już za daleko.'),
		say('j', 72, ' Oczywiście nie odpisuje też na żadne wiadomości.'),
		say('j', 73, ' Uparcie tkwi w tym swoim dziecinnym, obrażonym milczeniu.'),
		say('j', 74, ' Ale to nic, to nie powód, żeby się zrażać.'),
		say('j', 75, ' Niedługo na pewno zrozumie, że to już przestało być śmieszne i wreszcie odpisze. No, albo przynajmniej wyświetli. Naprawdę, skandal!'),
		'show character j sadNormalClothes at center',
		say('j', 76, ' I właśnie dlatego przestałam wracać do domu drogą, którą zawsze szłyśmy razem. Żeby nie dawać jej dodatkowego powodu do satysfakcji.'),
		say('j', 77, ' Owszem, nadkładam przy tym trochę drogi. Ale to nic.'),
		say('j', 78, ' W ten sposób przynajmniej nie mijam jej domu, a co za tym idzie, spada też szansa, że Charlie mnie zobaczy, ukryta gdzieś za strategicznie zakrytą firanką.'),
		'hide character j sadNormalClothes at center',
		'show scene schoolEntrance',
		say('j', 79, ' W szkole jest ciężko.'),
		say('j', 80, ' Rozniosła się plotka, jakobym to ja była winna temu, że Charlie jest taka nieodpowiedzialna i wybiera dziecinne zabawy zamiast edukacji.'),
		say('j', 81, ' Nie pomaga to, że nigdy nie byłam specjalnie lubiana.'),
		'show scene hallwaySchool',
		'show character j sadSchoolUniform at center',
		say('j', 82, ' No właśnie, wydaje mi się, że dziewczyny zawsze trochę się mnie bały.'),
		say('j', 83, ' Łatka modelki, pewna siebie, powodzenie u chłopaków, bla bla bla. Tak naprawdę niezbyt mnie interesowali – po co interesować się chłopakami, skoro wokół jest tyle ładnych dziewczyn? – a to ich nachalne zainteresowanie było właściwie powodem, dla których zaczęłam starać się ich unikać.'),
		say('j', 84, ' Umówmy się, łamanie serc nie jest przyjemne dla żadnej ze stron, nie mówiąc już o głupich docinkach czy gwizdach. Nawet jeśli wielokrotnie to jedynie mnie maluje się jako zimną sukę bez uczuć.'),
		say('j', 85, ' Do tego dochodzą jeszcze te napisy na szafkach. Naprawdę, czy jesteśmy w 2007 roku w amerykańskim lice...'),
		'hide character j',
		'show character u1 maliciousSchoolUniform at rightish',
		'show character u2 maliciousSchoolUniform at leftish',
		stopVoice(),
		hikari(1),
		'u1 Serio? To naprawdę okropne...',
		stopVoice(),
		yudeko(1),
		'u2 Ja w sumie nie jestem zaskoczona. Pamiętasz, jak w zeszłym miesiącu potraktowała Emila z 1A? Suka. Wcale się nie dziwię, że była w stanie doprowadzić kolejną osobę na skraj wytrzymałości psychicznej...',
		'show character j angrySchoolUniform at center',
		'show character u1 upsetSchoolUniform at rightish',
		'show character u2 sadSchoolUniform at leftish',
		'j ...',
		'hide character j',
		stopVoice(),
		hikari(2),
		'u1 Przepraszam...',
		'jump Chapter3'
	],

	Chapter3: [
		'show scene #000 with fadeIn',
		'wait 1500',
		'show scene hallwayHouse',
		'show character j sadSchoolUniform at center',
		'“Też powinnaś się zabić.”',
		'...',
		say('j', 86, 'Nie powiem, czytywałam już bardziej wysublimowane listy wrzucane do szkolnej szafki. Na przykład od tego chłopaka, miesiąc temu – widać było, że bardzo się postarał.'), // *westchnienie smutku*
		say('j', 87, 'Jak zwykle na odmowę nie było dobrego sposobu. Stałam tam tylko z oczami wbitymi w ziemię i patrzyłam, jak próbuje zachować resztę dumy i nie rozpłakać się jak dziecko... Cholerne listy miłosne.'),
		say('j', 88, 'A Charlie... Charlie wciąż milczy.'),
		say('j', 89, 'Szkoła bez niej stała się jeszcze bardziej nieznośna. Zresztą, szkoła... jutro chyba sobie odpuszczę. I tak nie uczą nas tam niczego przydatnego. Dzień odpoczynku dobrze mi zrobi.'),
		say('j', 90, 'A jeśli poszła po rozum do głowy i jutro wreszcie przyjdzie? To już miesiąc. Nie odpowiedziała do tej pory na żadną wiadomość. Więc jeśli jutro... No trudno, najwyżej będzie musiała trochę poczekać z przeprosinami. Zobaczy, jak to jest.'),
		say('j', 91, 'Zresztą, jutro, pojutrze... zaległości zawsze można odrobić.'),
		say('j', 92, 'Ciekawe, czy stronę, którą Charlie przysłała mi podczas wyjazdu na sesję, da się teraz otworzyć... “Pamiętnik Charlie”. Wtedy chroniło ją hasło... Zobaczymy.'),
		'play sound keyboardLogin',
		'wait 3500',
		'show character j fullSmileSchoolUniform at center',
		say('j', 93, 'Bingo! Zobaczmy, są chronologicznie... 20 marca, dzień, w którym się poznałyśmy. O, a potem 25, nasz pierwszy wspólny spacer...'),
		'jump Choice1Question'
	],

	Choice1Question: [
		{
			'Choice': {
				'March20': {
					'Text': 'wpis z 20 marca',
					'Do': 'jump Choice1March20'
				},
				'March25': {
					'Text': 'wpis z 25 marca',
					'Do': 'jump Choice1March25'
				}
			}
		}
	],

	Choice1March20: [
		'show scene hallwaySchool',
		'show character c stressedSchoolUniform at center',
		say('c', 1, 'Boże, o co chodzi tym ludziom? Czy oni nie rozumieją, że takie tłoczenie się jest niebezpieczne?'),
		say('c', 2, 'Ech, ta szkoła zdecydowanie nie należy do najprzyjemniejszych miejsc na ziemi.'),
		'show character c shockedSchoolUniform at center',
		say('c', 3, 'Hm? Ten dźwięk... Ktoś się przewrócił?'),
		'show character c shockedSchoolUniform at rightish',
		'show character j angrySchoolUniform at leftish',
		say('j', 94, 'Hej, nie widzisz, gdzie idziesz? Mogłeś jej coś zrobić!'),
		'show character j sadSchoolUniform at leftish',
		say('j', 95, 'Wszystko w porządku, jesteś cała? Możesz wstać?'),
		'show character c fullSmileSchoolUniform at rightish',
		say('c', 4, 'Awwwww... Nie wiedziałam, że jest taka miła, zawsze wydawała się zimna i trochę... nieobecna. I do tego jest taka ładna...'),
		'show character j fullSmileSchoolUniform at leftish',
		'show character c blushSchoolUniform at rightish',
		ruriri(40),
		'c O nie, patrzy w tę stronę! Zaraz zobaczy, że się gapię...',
		say('c', 5, 'Umm... Cześć, jestem... Charlie.'),
		'jump Chapter4',
	],

	Choice1March25: [
		'show scene kitchenNight',
		'stop music with fade 1',
		'wait 800',
		'play music sadDay with loop fade 1',
		'm CIĄGLE TYLKO PRETENSJE, MAM JUŻ DOSYĆ WYSŁUCHIWANIA, ŻE TO JEDYNIE TY SIĘ STARASZ!',
		'm JESZCZE ROBISZ TO TYM SWOIM WSZECHWIEDZĄCYM TONEM, CHODZISZ I POUCZASZ INNYCH, MYŚLISZ ŻE MASZ TAKIE PRAWO?',
		'm JESTEŚ SKRAJNIE WKURZAJĄCY, NIE MOGĘ JUŻ CIĘ SŁUCHAĆ, ZOSTAW MNIE W SPOKOJU.',
		'o NIE BĘDZIESZ MI MÓWIĆ, CO MAM ROBIĆ! ZWŁASZCZA ŻE ZWYKLE NIE PRZYKŁADASZ NAWET PALCA DO CZEGOKOLWIEK W DOMU, TYLKO PRODUKUJESZ JESZCZE WIĘKSZY BURDEL! JEŚLI CHCESZ ŻYĆ W CHLEWIE, TO RÓB GO W SWOIM POKOJU!',
		'o CHARLIE, CZEMU ZNOWU WRACASZ TAK PÓŹNO?! CZY JUŻ CI NIE MÓWIŁEM, ŻE MASZ SIĘ NIE SZWENDAĆ PO NOCACH?!',
		'show character c irritatedSchoolUniform at center',
		say('c', 6, 'Nie twój interes...'),
		'o JAKIM TONEM ODPOWIADASZ OJCU?! MAM CI PRZYPOMNIEĆ, KTO ZAJMUJE SIĘ TWOIM UTRZYMANIEM?! ODPOWIADAJ, JAK CIĘ PYTAM?!',
		'show character c angrySchoolUniform at center',
		say('c', 7, 'WRÓCIŁAM PÓŹNO, BO POSZŁAM JESZCZE Z KOLEŻANKĄ NA SPACER, OKEJ?! A TERAZ ZOSTAWCIE MNIE WRESZCIE W SPOKOJU! I TAK WIEM, ŻE NIC WAS TO NIE OBCHODZI!!!!!'),
		'play sound slammingTheDoor',
		'show scene bedroomNight',
		'show character c sadSchoolUniform at center',
		say('c', 8, 'Boże, nienawidzę tego... nienawidzę ich... Jak długo jeszcze będę musiała to przechodzić...?'), // po kwestii mówionej odgłos tego, że zbiera jej się coraz bardziej na płacz
		'show character c sadWithTearsSchoolUniform at center',
		'jump Chapter4',
	],

	Chapter4: [
		'show scene #000 with fadeIn',
		stopVoice(),
		'stop music with fade 1',
		'wait 1000',
		'play music deepBlue with loop fade 1',
		'show scene busStop',
		'show character j sadNormalClothes at center',
		aloes(96),
		'j Czy ktoś... za mną idzie?',
		say('j', 97, 'Nie, to na pewno tylko złudzenie.'),
		say('j', 98, 'A jeśli nie? Mogłabym przysiąc, że słyszę kroki...'),
		say('j', 99, 'Może... może jednak wrócę do domu?'),
		'show scene hallway',
		'<em>“Masz krew na rękach.”</em>',
		'<em>“Znalazłaś sobie nową ofiarę?”</em>',
		'<em>“Zabiłaś Emila, szmato. Popamiętasz.”</em>',
		'<em>“Też powinnaś się zabić. Możemy pomóc.”</em>',
		'show scene hallwayHouse',
		'show character j worriedNormalClothes at center',
		say('j', 100, 'Idiotyzm. Opowiem o tym Charlie, jak tylko sprawy wrócą do normy. Nie, żeby to nie zdarzało mi się wcześniej od czasu do czasu.'),
		say('j', 101, 'Ale wtedy przynajmniej miałam komu się wygadać. A te groźby... to nic. Niech sobie krzyczą. Mało mnie to rusza.'),
		say('j', 102, 'Zresztą, w szkole nic mi nie zrobią. W końcu to szkoła. Ciekawe, czy Charlie już wróciła...'),
		say('j', 103, 'Miała już okazję podczas tych kilku dni mojej nieobecności.'),
		say('j', 104, 'Kilku? Hm, a jednak, wygląda na to, że kilkunastu. Nawet nie zauważyłam, kiedy to zleciało.'),
		say('j', 105, 'W każdym razie wychowawczyni zmęczyła się chyba brakiem odzewu i telefon zamilkł w zeszłym tygodniu. W końcu. Teraz wreszcie mogę w spokoju odpocząć.'),
		say('j', 106, 'Właśnie, ostatnio brakuje mi pozytywnych myśli. To zdecydowanie niezdrowe. Od zbyt długiego zamartwiania się można... stracić dobrą kondycję psychiczną. Nie możemy do tego dopuścić!'),
		say('j', 107, 'Jak tylko cała ta sytuacja się skończy, muszę przecież być w pełni sił. Okładki w końcu same się nie zapozują. Pomyślmy, może jakieś miłe wspomnienie...'),
		'show character j smileNormalClothes at center',
		'stop music with fade 3',
		say('j', 108, 'O, no tak. Pamiętne nocowanie, podczas którego się zeszłyśmy. To zdecydowanie jest przyjemne wspomnienie.'),
		say('j', 109, 'Cała sytuacja była dość zabawna, ale mimo wszystko wyszła chyba dość naturalnie? Już jakiś czas nosiłam się wtedy z myślą, jak wybadać sytuację, a w sumie Charlie koniec końców mnie w tym wyręczyła. Kochana Charlie. Hm.'), // *uśmiech do siebie, takie uśmiechnięte hm, dobre wspomnienia*.
		say('j', 110, 'Ciekawe, jak to wyglądało z jej strony.'),
		'jump Choice2Question'
	],

	Choice2Question: [
		{
			'Choice': {
				'May02Afternoon': {
					'Text': 'wpis z 2 maja, popołudnie',
					'Do': 'jump Choice2Afternoon'
				},
				'May02Evening': {
					'Text': 'wpis z 2 maja, wieczór',
					'Do': 'jump Choice2Evening'
				}
			}
		}
	],

	Choice2Evening: [
		'show scene bedroomNight',
		'stop music',
		'play music memories with loop fade 1',
		'show character c excitedPJ at leftish',
		say('c', 9, 'Oczywiście nie jestem w tym jeszcze jakaś superdobra, ale codziennie ćwiczę i wydaje mi się, że idzie mi coraz lepiej? Próbowałam nawet napisać tekst do tej melodii, którą ułożyłam w zeszłym tygodniu, ale zupełnie nie mogę znaleźć rymu w jednym miejscu i totalnie mi to nie daje spokoju, mówię ci, ostatnio zastanawiałam się nad nim pół godziny i myślałam, że głowa mi...'), // TODO rozdzielić
		'show character j smirkPJ at rightish',
		say('j', 111, 'Hyhyhy.'),
		'show character c bububuPJ at leftish',
		say('c', 10, 'Hej, czemu się ze mnie śmiejesz? Ty ty ty...'),
		say('j', 112, 'Awwwww. Nie śmieję się, po prostu zwykle mało mówisz. Zapominam, jaka jesteś urocza, kiedy się czymś podekscytujesz.'),
		'show character c blushPJ at leftish',
		say('c', 11, 'Ugh... Może... Może to dlatego, że jesteś jedyną osobą, którą ciekawi to, co mówię i jedynie przy tobie mogę się naprawdę wyluzować... Naprawdę to doceniam, wiesz?'),
		'show character j sadPJ at rightish',
		say('j', 113, 'Hm? Em, mogłabyś powtórzyć trochę głośniej? Nie usłyszałam...'),
		say('c', 12, 'Ugh... Mówię tylko, że najbardziej lubię spędzać czas właśnie z tobą i... Nasze spotkania to zawsze najprzyjemniejsza część dnia i... mogłabymtakjużnastałeojeśliczywiścieteżbyśchciałaalepewnieniechcesznieważne.'),
		'show character j shockedPJ at rightish',
		say('j', 114, 'Och...'),
		say('c', 13, '...'),
		'show character j fullSmilePJ at rightish',
		say('j', 115, 'Bardzo bym tego chciała!'),
		'show character c shockedPJ at leftish',
		'show character c smilePJ at leftish',
		say('c', 14, '...'),
		'jump Chapter5'
	],

	Choice2Afternoon: [
		stopVoice(),
		'stop music with fade 1',
		'play music sadDay with loop fade 1',
		ruriri(15),
		'show scene bedroom',
		'show character c sadPJ at center',
		'c Do niczego... jestem do niczego. Ta linijka wciąż brzmi źle, mimo że zastanawiam się nad nią przez godzinę... Zresztą, czegokolwiek nie wymyślę, pewnie będzie do niczego.',
		say('c', 37, 'Bezużyteczna. Beztalencie. Brzydka. Nikogo nie obchodzi to, co robię. W ogóle nikogo nie obchodzę... Ciekawe, czy gdybym coś sobie zrobiła, to ktoś by zauważył. Jasne... Powinnam zniknąć i przestać przysparzać innym kłopotu.'),
		say('c', 38, 'Jo jest dla mnie taka dobra... Nie zasługuję na nią. Jak tylko zobaczy, jaka jestem naprawdę, też zacznie mnie nienawidzić. Tak jak ja. No bo co miałaby zobaczyć w tak bezwartościowym człowieku jak ja? Nic. No właśnie. Może powinnam ją wyręczyć i sama to zakończyć... Tak naprawdę na pewno ją męczę, tylko nie wie, jak mi to powiedzieć.'), // TODO rozdzielić
		say('c', 39, 'Jo jest miła. Nie jest osobą, która mówi innym takie rzeczy wprost, ale... nie chcę tego kończyć... bez niej już nic mi nie zostanie...'),
		'show character c sadWithTearsPJ at center',
		'play sound softKnock',
		'play sound openingTheDoor',
		'show character c sadWithTearsPJ at leftish',
		'show character j fullSmilePJ at rightish',
		say('j', 116, 'Hej Charlie, przyszłam trochę wcześniej, bo chciałam ci zrobić niespodziankę! Charlie, wszystko w porządku?'),
		'show character c gloomyPJ at leftish',
		'show character j sadPJ at rightish',
		say('c', 17, '...'),
		say('j', 117, 'Co się stało? Dlaczego płaczesz...?'),
		'show character c sadWithTearsPJ at leftish',
		'jump Chapter5',
	],

	Chapter5: [
		'show scene #000 with fadeIn',
		'stop music with fade 2',
		'wait 2000',
		'show scene livingRoomNight with fadeIn',
		'play music deepBlue with loop fade 3',
		'show character j neutralNormalClothes at center',
		say('j', 118, 'Właściwie, jeśli nie chodzę chwilowo do szkoły... jaki jest sens wychodzić w ogóle? Tylko traci się na tym niepotrzebnie energię, a na dworze aura nie sprzyja.'),
		say('j', 119, 'Gdyby codziennie świeciło słońce, to co innego, ale pogoda nie jest zbyt przyjazna wychodzeniu. Wręcz przeciwnie – to pogoda tego typu, podczas której można łatwo złapać przeziębienie. A przynajmniej tak zakładam.'),
		say('j', 120, 'Od czasu, kiedy znalazłam na szybach ślady po rozbitych jajkach, zaciągnęłam wszystkie zasłony. To o wiele szybszy i bardziej ekonomiczny sposób na pozbycie się problemu brudnych okien niż ich umycie.'),
		'show character j sadNormalClothes at center',
		say('j', 121, 'A tak jest mi dobrze. Nawet jeśli w międzyczasie zaczęło świecić słońce. Zresztą, zbyt duża ekspozycja na promienie słoneczne zwiększa ryzyko raka skóry.'),
		say('j', 122, 'Na to nie mogę sobie pozwolić. Przecież jestem modelką.'),
		say('j', 123, 'Tylko... chwilowo robię sobie przerwę. Tak, to tylko mała przerwa, niedługo wszystko wróci do normy.'),
		say('j', 124, 'Prawda, Charlie?'),
		'show scene #000 with fadeIn',
		'wait 1000',
		'show scene hallwayHouseNight with fadeIn',
		'show character j worriedNormalClothes at center',
		say('j', 125, 'Okej, zrobione. Drzwi do łazienki zamurowa... szczelnie zamknięte.'),
		say('j', 126, 'Kiedyś lubiłam długie kąpiele w ciepłej wodzie. Teraz jakoś... sama myśl o tym przyprawia mnie o dreszcze. Zresztą, najważniejsze jest bezpieczeństwo. A kratka wentylacyjna to świetny sposób na wejście komuś do mieszkania. Jak na filmach.'),
		say('j', 127, 'Ale skoro pokazują to w prawie każdym filmie akcji, to musi się w tym kryć przynajmniej cząstka prawdy, prawda? Przecież inaczej to nie miałoby sensu...'),
		say('j', 128, 'Ostatnie dwa wpisy... pierwszy, z naszego ostatniego spotkania przed tym głupim obrażaniem się, które trwa już trzeci miesiąc, i drugi, z czasu, kiedy Charlie została tu sama.'),
		'show character j sadNormalClothes at center',
		say('j', 129, 'Hej, dlaczego... moje ręce tak drżą...?'),
		'jump Choice3Question'
	],

	Choice3Question: [
		{
			'Choice': {
				'March05': {
					'Text': 'wpis z 5 marca',
					'Do': 'jump Choice3March05'
				},
				'March12': {
					'Text': 'wpis z 12 marca',
					'Do': 'jump Choice3March12'
				}
			}
		}
	],

	Choice3March05: [
		'stop music with fade 1',
		'wait 1000',
		'play music sadDay with loop fade 1',
		'show scene park with fadeIn',
		'show character j worriedNormalClothes at leftish',
		say('j', 130, 'Przepraszam, mogłam... mogłam powiedzieć ci wcześniej. Powinnam była powiedzieć ci wcześniej. Ale to tylko dwa tygodnie, ani się obejrzysz, a będę z powrotem.'),
		'show character c sadEyesDownNormalClothes at rightish',
		say('c', 18, 'Czy to... dlatego, że masz już mnie dość? Szukasz wymówki, żeby tylko nie musieć się ze mną widywać?'),
		stopVoice(),
		aloes(131),
		'show character j worriedNormalClothes at leftish',
		'j Charlie, przestań. Spójrz na mnie. Wszystko będzie dobrze. Po prostu przez kilka dni zamiast rozmawiać twarzą w twarz będziemy rozmawiać przez kamerki. Nic się nie zmienia. Nadal chcę z tobą być.',
		'show character c sadderEyesDownNormalClothes at rightish',
		say('c', 19, 'Nie dam sobie rady sama. To jest zbyt silne. Zbyt... straszne. Ja... nie potrafię.'),
		stopVoice(),
		aloes(132),
		'show character j sadNormalClothes at leftish',
		'j Proszę, bądź silna. Zrób to dla mnie. I dla siebie. Wierzę, że ci się uda. Niedługo wrócę i znów wszystko będzie dobrze.',
		'show character c cryingNormalClothes at rightish',
		say('c', 20, '...'),
		'jump Choice3After'
	],

	Choice3March12: [
		'stop music with fade 1',
		'wait 1000',
		'play music sadDay with loop fade 1',
		'show scene bedroom',
		'show character c livelessNormalClothes at center',
		say('c', 21, 'Wiedziałam...'),
		say('c', 22, 'To pewnie tylko wymówka...'),
		say('c', 23, 'Nie mogła już wytrzymać, a nie chciała mnie ranić, mówiąc mi to prosto w twarz...'),
		say('c', 24, 'A nawet jeśli nie... to nie mam już siły... nie dam sobie rady bez niej...'),
		say('c', 25, 'Jak mam wytrzymać z tym wszystkim sama? Już chyba wolę po prostu zakończyć to raz na zawsze...'),
		say('c', 26, 'Ciekawe, jak szybko zorientują się rodzice...'),
		say('c', 27, 'Pewnie nawet jakbym krzyczała z bólu, to nic by nie słyszeli, pochłonięci własną kłótnią...'),
		stopVoice(),
		ruriri(28),
		'show character c afraidNormalClothes at center',
		'c Ciekawe, jak bardzo to boli... Mam nadzieję, że to tylko chwila... a jeśli nie... a jeśli mnie znajdą...',
		stopVoice(),
		ruriri(29),
		'show character c cryingNormalClothes at center',
		'c Jak ja spojrzę wtedy Jo w oczy? Nie, już trudno, decyzja zapadła...',
		say('c', 30, 'Już... wystarczy.'),
		'jump Choice3After'
	],

	Choice3After: [
		'show scene #000 with fadeIn',
		'wait 1000',
		'show scene livingRoomNight with fadeIn',
		say('c', 31, '"Cześć, tu Charlie. Jeśli to słyszysz, to znaczy, że chwilowo nie mogę rozmawiać. Zostaw wiadomość, przesłucham ją, kiedy tylko będę mogła. Dziękuję!"'),
		'show character j sadNormalClothes at center',
		say('j', 133, 'Charlie, proszę... odezwij się... proszę... Tak bardzo się boję... Oni tu mogą przyjść w każdej chwili... Poza tym nie słyszałam cię już tak dawno... Tęsknię, wiesz...?'),
		say('j', 134, 'Bardzo... Proszę, napisz chociaż jedno słowo... Proszę... Przecież nie popełniłaś samobójstwa, prawda? Nie zostawiłaś mnie całkiem samej? Prawda?'),
		'jump Ending'
	],

	Ending: [
		'show scene #000 with fadeIn',
		'stop music with loop fade 1',
		'wait 1000',
		'show scene hallwayHouseNight with fadeIn',
		'play sound louderKnock',
		'wait 1000',
		'play music scifi with loop fade 5',
		'show character j terrifiedNormalClothes at center',
		'...',
		say('j', 135, 'To nie może się tak skończyć.'),
		say('j', 136, 'Proszę.'),
		say('j', 137, 'Proszę...'),
		say('j', 138, 'Łańcuch. Łańcuch ich zatrzyma. A może mają siekierę? Przecież to głupie, ktoś na pewno by ich zauważył. Chyba, że już wieczór... Tak, na pewno jest już noc. W takim razie klatka schodowa jest całkiem pusta... I nikt nie usłyszy moich krzyków o pomoc.'),
		say('j', 139, 'Jeśli w ogóle będę w stanie jakiś z siebie wydać. A jeśli... wydam z siebie tylko ciche westchnienie... wtedy nikt mnie nie usłyszy... nikt a nikt... Zostanę tu, z podłogą obryzganą krwią... martwa... z sinymi dłońmi i otwartymi ustami...'),
		'play sound loudKnock',
		'...',
		'show character j sadNormalClothes at center',
		say('j', 140, 'A jeśli...'),
		say('j', 141, 'A jeśli to Charlie?'),
		say('j', 142, 'Która przyszła mnie ocalić?'),
		say('j', 143, 'Wreszcie znudził jej się ten głupi żart...'),
		say('j', 144, 'Zrozumiała, że coś się stało i przyszła powiedzieć, że wszystko jest w porządku...'),
		say('j', 145, 'Kochana Charlie...'),
		say('j', 146, 'Może otworzę drzwi tylko odrobinę...?'),
		say('j', 147, 'Może będzie tam...?'),
		'show character j worriedNormalClothes at center',
		say('j', 148, 'A jeśli nie...?'),
		say('j', 149, 'Czy warto ryzykować?'),
		say('j', 150, 'Jeśli to naprawdę oni?'),
		say('j', 151, 'Przecież...'),
		say('j', 152, 'Charlie...'),
		'jump Choice4Question'
	],

	Choice4Question: [
		{
			'Choice': {
				'Stay': {
					'Text': 'Zostań w miejscu',
					'Do': 'jump Choice4Stay'
				},
				'Open': {
					'Text': 'Otwórz drzwi',
					'Do': 'jump Choice4Open'
				}
			}
		}
	],

	Choice4Stay: [
		'...',
		'......',
		'.........',
		'show scene end with fadeIn duration 5s',
		'stop music with fade 5',
		'<span class="middle"><em>Koniec</em></span>',
		'end'
	],
	Choice4Open: [
		'play sound slowDoorOpening',
		'show scene #fff with fadeIn',
		'stop music with fade 2',
		'wait 2000',
		'show scene hallway with fadeIn',
		'show character u2 sadSchoolUniform at center',
		stopVoice(),
		hikari(3),
		'u1 Hej, ja... chciałabym przeprosić za wtedy, a od kilku tygodni nie ma cię w szkole i pomyślałam, że...',
		stopVoice(),
		hikari(4),
		'u1 O mój Boże, co się stało? Słyszysz mnie? Wszystko... Jo, słuchaj mnie. Jo. Zostań ze mną. Jo...',
		stopVoice(),
		hikari(5),
		'u1 Przepraszam, chciałabym zgłosić nagły przypadek...',
		'<span class="middle"><em>Koniec</em></span>',
		'end'
	],
});
