/**
 * ============================================================
 * Polish
 * ============================================================
 *
 * Translators:
 *
 * Orzecho <orzecho6@gmail.com>
 */



export default {
	'AdvanceHelp': 'Kliknij lewym klawiszem myszy lub wciśnij spację by przejść do następnego kroku.',
	'AllowPlayback': 'Kliknij tutaj by włączyć odtwarzanie audio',
	'Audio': 'Audio',
	'AutoPlay': 'Auto',
	'AutoPlayButton': 'Włącz auto odtwarzanie',
	'AutoPlaySpeed': 'Prędkość auto odtwarzania',

	'Back': 'Cofnij',
	'BackButton': 'Cofnij',

	'Cancel': 'Anuluj',
	'Close': 'Zamknij',
	'Confirm': 'Czy na pewno chcesz wyjść?',
	'Credits': 'Lista płac',

	'Delete': 'Usuń',
	'DialogLogButton': 'Pokaż log',

	'FullScreen': 'Pełny ekran',

	'Gallery': 'Galeria',

	'Help': 'Pomoc',
	'Hide': 'Ukryj',
	'HideButton': 'Ukryj tekst',

	'iOSAudioWarning': 'Ustawienia audio nie są wspierane na iOS',

	'KeyboardShortcuts': 'Skróty klawiszowe',

	'Language': 'Język',
	'Load': 'Wczytaj',
	'LoadAutoSaveSlots': 'Auto zapisane gry',
	'LoadButton': 'Otwórz ekran wczytywania',
	'Loading': 'Wczytywanie',
	'LoadingMessage': 'Zasoby są wczytywane, poczekaj proszę',
	'LoadSlots': 'Zapisane gry',
	'LocalStorageWarning': 'Local Storage nie jest dostępne na tej przeglądarce',
	'Log': 'Log',

	'Music': 'Głośność muzyki',

	'NewContent': 'Gra została zaktualizowana, przeładuj stronę by załadować najnowszą wersję',
	'NoSavedGames': 'Brak zapisanych gier',
	'NoAutoSavedGames': 'Brak automatycznie zapisanych gier',
	'NoDialogsAvailable': 'Brak dialogów. Dialogi pojawią się tutaj',

	'OK': 'OK',
	'OrientationWarning': 'Proszę, obróć urządzenie',
	'Overwrite': 'Nadpisz',

	'QuickButtons': 'Szybkie menu',
	'QuickMenu': 'Szybkie menu',
	'Quit': 'Wyjdź',
	'QuitButton': 'Wyjdź z gry',

	'Resolution': 'Rozdzielczość',

	'Save': 'Zapisz',
	'SaveButton': 'Otwórz ekran zapisu',
	'SaveInSlot': 'Zapisz w slocie',
	'Settings': 'Ustawienia',
	'SettingsButton': 'Otwórz ekran ustawień',
	'Show': 'Pokaż',
	'Skip': 'Pomiń',
	'SkipButton': 'Wejdź w tryb pomijania',
	'SlotDeletion': 'Czy na pewno chcesz usunąć ten slot?',
	'SlotOverwrite': 'Czy na pewno chcesz nadpisać ten slot?',
	'Sound': 'Głość dźwięków',
	'Start': 'Start',
	'Stop': 'Stop',

	'TextSpeed': 'Prędkość tekstu',

	'Video': 'Głośność wideo',
	'Voice': 'Głośność dubbingu',

	'Windowed': 'W oknie'
};
